// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "BaseCoin.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam( FOnPlayerColition , float , timeAdded);

UCLASS()
class COINCOLLECTOR_API ABaseCoin : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseCoin();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle DeathTimerHandle;

	void DeathTimerComplete();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly )
	USceneComponent* Root;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly )
	UStaticMeshComponent* CoinMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RotationRate;

	UPROPERTY( BlueprintAssignable, Category = "EventDispatchers" )
	FOnPlayerColition playerColition;

	UFUNCTION(BlueprintCallable)
	void PlayCustomDeath();

	UFUNCTION(BlueprintNativeEvent)
	void onOverlap( AActor* OverlappedActor, AActor* OtherActor );

private:

	bool m_hasBeenTouched;

};
