// Fill out your copyright notice in the Description page of Project Settings.


#include "GameManager.h"
#include "OrbitActor.h"
#include "EngineUtils.h"
#include "EngineGlobals.h"

// Sets default values
AGameManager::AGameManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_TimeLeft = 13;
	m_Score = 0;

	m_GameState = gameState::Gameplay;

}

void AGameManager::AddScore()
{
	m_Score++;
}

void AGameManager::AddTime( float time )
{
	m_TimeLeft += time;
}

void AGameManager::timeLeftCounter()
{

	--m_TimeLeft;

	if ( m_TimeLeft <= 0 )
	{
		
		stopTimerCounter();

		m_GameState = gameState::Lose;

		GEngine->AddOnScreenDebugMessage( -1, 10, FColor::Red, "Timer's up!" );
	}

}

void AGameManager::coinPicked(float timeAdded)
{
	m_CoinsLeft--;
	
	AddScore();
	AddTime(timeAdded);

	checkWinLoseConditions();
}

void AGameManager::coinPickedByOtherActor()
{
	m_CoinsLeft--;

	checkWinLoseConditions();
}

void AGameManager::stopTimerCounter()
{
	GetWorldTimerManager().ClearTimer(m_CountdownTimerHandle);
}

void AGameManager::checkWinLoseConditions()
{

	if ( m_CoinsLeft <= 0 && m_Score >= m_CoinsToPickUp)
	{
		m_GameState = gameState::Win;

		GEngine->AddOnScreenDebugMessage( -1, 10, FColor::Red, "Win");
		
		stopTimerCounter();
	}
	else if ( m_CoinsLeft <= 0 && m_Score < m_CoinsToPickUp )
	{
		m_GameState = gameState::Lose;

		GEngine->AddOnScreenDebugMessage( -1, 10, FColor::Red, "Lose");

		stopTimerCounter();
	}

}

void AGameManager::delegateWorking()
{
	GEngine->AddOnScreenDebugMessage( -1, 10, FColor::Red, "Coin Collected");
}

// Called when the game starts or when spawned
void AGameManager::BeginPlay()
{
	Super::BeginPlay();
	
	for (TActorIterator<ABaseCoin> It(GetWorld()); It; ++It)
     {
		
		It->playerColition.AddDynamic( this, &AGameManager::coinPicked);
		m_Coins.Add(*It);
		
     }

	for (TActorIterator<AOrbitActor> It(GetWorld()); It; ++It)
     {
		
		It->m_CoinCollision.AddDynamic( this, &AGameManager::coinPickedByOtherActor);
		
     }

	m_CoinsLeft = m_Coins.Num();

	m_CoinsToPickUp = (m_CoinsLeft/2) + 1;

	GetWorldTimerManager().SetTimer( m_CountdownTimerHandle, this, &AGameManager::timeLeftCounter, 0.5f, true, 0.5f );
}

// Called every frame
void AGameManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

