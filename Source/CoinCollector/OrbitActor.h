// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseCoin.h"
#include "GameFramework/Actor.h"
#include "OrbitActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCoinTouched);

UCLASS()
class COINCOLLECTOR_API AOrbitActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOrbitActor();

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly )
	UStaticMeshComponent* m_Mesh;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly )
	AActor* m_Owner;

	float m_AngleAxis;

	UPROPERTY( EditAnywhere, Category = "Movement" )
	FVector m_Dimensions;

	UPROPERTY( EditAnywhere, Category = "Movement" )
	FVector m_AxisVector;

	UPROPERTY( EditAnywhere, Category = "Movement" )
	float m_Multiplier;

	UPROPERTY( EditAnywhere, Category = "EventDispatchers" )
	FOnCoinTouched m_CoinCollision;

	UFUNCTION(BlueprintNativeEvent)
	void onOverlap( AActor* OverlappedActor, AActor* OtherActor );

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	bool m_hasBeenTouched;
	ABaseCoin* m_CoinTouchedReceantly;
};
