// Fill out your copyright notice in the Description page of Project Settings.


#include "OrbitActor.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AOrbitActor::AOrbitActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));

	RootComponent = m_Mesh;

	m_Mesh->SetGenerateOverlapEvents(true);
	m_Mesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	m_Mesh->SetNotifyRigidBodyCollision(true);

	OnActorBeginOverlap.AddDynamic( this,&AOrbitActor::onOverlap );

	m_Dimensions = FVector ( 220, 0, 0 );
	m_AxisVector = FVector( 0, 0, 1 );
	m_Multiplier = 80.0f;

}

// Called when the game starts or when spawned
void AOrbitActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AOrbitActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector newLocation =  GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();// m_Owner->GetActorLocation();

	m_AngleAxis += DeltaTime * m_Multiplier;

	if ( m_AngleAxis >= 360.0f )
	{
		m_AngleAxis = 0.0f;
	}

	FVector rotateValue = m_Dimensions.RotateAngleAxis(m_AngleAxis, m_AxisVector);

	newLocation.X += rotateValue.X;
	newLocation.Y += rotateValue.Y;
	newLocation.Z += rotateValue.Z;

	FRotator newRotation = FRotator( 0, m_AngleAxis, 0 );

	FQuat quatRotation = FQuat(newRotation);

	SetActorLocationAndRotation( newLocation, quatRotation, false, 0, ETeleportType::None );
}

void AOrbitActor::onOverlap_Implementation( AActor* OverlappedActor, AActor* OtherActor )
{
	if (Cast<ABaseCoin>(OtherActor) != nullptr && m_CoinTouchedReceantly != Cast<ABaseCoin>(OtherActor))
	{
		Cast<ABaseCoin>( OtherActor )->PlayCustomDeath();

		m_CoinCollision.Broadcast();

		m_hasBeenTouched = true;

		m_CoinTouchedReceantly = Cast<ABaseCoin>( OtherActor );
	}
}