// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/Array.h"
#include "BaseCoin.h"
#include "GameManager.generated.h"


UENUM(BlueprintType)
enum gameState
{
	Gameplay UMETA( DisplayName = "Playing" ),
	Win      UMETA( DisplayName = "Win" ),
	Lose     UMETA( DisplayName = "Lose" ),
};

UCLASS()
class COINCOLLECTOR_API AGameManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameManager();

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly )
	int m_Score;

	UPROPERTY( VisibleAnywhere, BlueprintReadWrite )
	int m_CoinsLeft;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly )
	int32 m_TimeLeft;

	UPROPERTY( VisibleAnywhere, BlueprintReadWrite )
	int32 m_CoinsToPickUp;

	UPROPERTY( EditAnywhere, BlueprintReadOnly, Category = "GameState" )
	TEnumAsByte<gameState> m_GameState;

	FTimerHandle m_CountdownTimerHandle;

	UFUNCTION(BlueprintCallable)
	void AddScore();

	UFUNCTION(BlueprintCallable)
	void AddTime(float time);

	TArray<ABaseCoin*> m_Coins;

	void timeLeftCounter();

	UFUNCTION()
	void coinPicked( float timeAdded);

	UFUNCTION()
	void coinPickedByOtherActor();

	UFUNCTION()
	void delegateWorking();

	UFUNCTION( BlueprintCallable )
	void stopTimerCounter();

	void checkWinLoseConditions();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
